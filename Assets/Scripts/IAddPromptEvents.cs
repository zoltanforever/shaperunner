﻿
public delegate void AdPromptEvent();

interface  IAddPromptEvents
{
    event AdPromptEvent CancelClicked;
    event AdPromptEvent AdFinished;
}

