﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class AudioManager : MonoBehaviour {

    const float MIN_PITCH = 0.3f;
    const float MAX_PITCH = 1;

    AudioSource _audioSource;

    const float DO_NOT_SLOW_DOWN_TIME = 0.5f;
    float _timeOfLastShapeSelection;
   
    private void Awake()
    {
        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();
        IShapeEvents shapeEvents = GameObject.FindObjectOfType<ShapesController>();

        shapeEvents.FirstShapeSelected += PlayMusic;
        shapeEvents.CorrectShapeSelected += OnCorrectShapeSelected;

        gameEvents.Paused += StopMusic;
        
        _audioSource = GetComponent<AudioSource>();
        _audioSource.volume = GlobalSettings.Volume;

        _timeOfLastShapeSelection = Time.time;
    }

    private void PlayMusic()
    {
        _audioSource.Play();
        FakeTimeOfLastShapeSelection();
    }

    private void FakeTimeOfLastShapeSelection()
    {
        _timeOfLastShapeSelection = Time.time - TimerController.TIME_FOR_SHAPE_IN_SECONDS;
    }

    private void OnCorrectShapeSelected()
    {
        _timeOfLastShapeSelection = Time.time;
    }

    private float GetCurrentPeriod()
    {
        float t;
        if (Time.time - _timeOfLastShapeSelection < DO_NOT_SLOW_DOWN_TIME)
        {
            t = Time.time;
        } else
        {
            t = _timeOfLastShapeSelection;
        }    
        return (Time.time - t) / TimerController.TIME_FOR_SHAPE_IN_SECONDS;
    }

    private float PeriodToPitch(float period)
    {
        return MAX_PITCH - (MAX_PITCH - MIN_PITCH) * period;
    }

    private void Update()
    {
        if (_audioSource.isPlaying)
        {
            _audioSource.pitch = PeriodToPitch(GetCurrentPeriod());
        }
    }

    private void StopMusic()
    {
        _audioSource.Stop();
    }
}
