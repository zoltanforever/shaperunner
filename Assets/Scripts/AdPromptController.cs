﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;


public class AdPromptController : MonoBehaviour, IAddPromptEvents
{
    public string GameID = "1545225";
    public Transform AddPromptContainer;

    private const string PLACEMENT_ID = "rewardedVideo";

    public Button WatchAddButton;
    public Button CancelAddButton;

    public event AdPromptEvent CancelClicked;
    public event AdPromptEvent AdFinished;

    private bool _isAddShowed;

    private void Awake()
    {
        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();
        gameEvents.Paused += Show;
        gameEvents.GameOver += Hide;
    }

    private void Start()
    {
        Hide();
        CheckIfAdsAreSupportedOnPlatform();
        AddButtonListeners();
    }

    private void AddButtonListeners()
    {
        WatchAddButton.onClick.AddListener(ShowAd);
        CancelAddButton.onClick.AddListener(OnCancelClicked);
    }

    private void CheckIfAdsAreSupportedOnPlatform()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize(GameID, true);
        }
    }

    private void Update()
    {
        if (WatchAddButton)
        {
            WatchAddButton.interactable = Advertisement.IsReady(PLACEMENT_ID);
        }
    }

    private void Show()
    {
        if (!_isAddShowed)
        {
            _isAddShowed = true;
            AddPromptContainer.gameObject.SetActive(true);
        }
        else
        {
            IssueAdEvent(AdFinished);
        }
    }

    private void Hide()
    {
        AddPromptContainer.gameObject.SetActive(false);
    }

    private void OnCancelClicked()
    {
        Debug.Log("Cancel clicked");
        IssueAdEvent(CancelClicked);
        Hide();
    }

    private void ShowAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show(PLACEMENT_ID, options);
        Hide();
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            IssueAdEvent(AdFinished);

        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            IssueAdEvent(CancelClicked);
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            IssueAdEvent(CancelClicked);
        }
    }

    private void IssueAdEvent(AdPromptEvent eventToIssue)
    {
       if(eventToIssue != null)
        {
            eventToIssue();
        }
    }
}
