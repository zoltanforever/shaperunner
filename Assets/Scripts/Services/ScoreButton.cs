﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreButton : MonoBehaviour {

    private Button _button;
    private Text _text;
    private const string SCORE = "Score: ";
    public int Score;
    
	void Start () {
        Score = 0;
        _button = GetComponent<Button>();
        _button.onClick.AddListener(IncrementScore);
        _text = transform.GetComponentInChildren<Text>();
        UpdateScoreText();
    }

    private void IncrementScore()
    {
        Score++;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        _text.text = SCORE + Score;
    }
}
