﻿using UnityEngine;
using UnityEngine.UI;

public class Achieve : MonoBehaviour {

    private Button _button;

    private const string GLOBAL_ACHIEVEMNT_ID = "FIRST_ACHIEVEMENT";
    private const double PROGRESS_PERCENTAGE = 100;

    // Use this for initialization
    void Start () {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(ReportAchievemnt);
	}

    private void ReportAchievemnt()
    {
        // If its an incremental achievement, make sure you send a incremented cumulative value everytime you call this method
        NPBinding.GameServices.ReportProgressWithGlobalID(GLOBAL_ACHIEVEMNT_ID, PROGRESS_PERCENTAGE, OnComplete);
    }

    private void OnComplete(bool _success, string _error)
    {
        if (_success)
        {
            Debug.Log("Achievement report successful.");
        }
        else
        {
            Debug.Log("Achievement report failed beacuse " + _error);
        }
    }
}
