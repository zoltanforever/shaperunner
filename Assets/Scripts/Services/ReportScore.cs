﻿using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;

public class ReportScore : MonoBehaviour {

    private Button _button;
    private const string GLOBAL_LEADERBOARD_ID = "FIRST_LEADERBOARD";
    private ScoreButton _scoreButton;

    void Start () {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(ReportScores);
        _scoreButton = FindObjectOfType<ScoreButton>();
    }

    private void ReportScores()
    {
        NPBinding.GameServices.ReportScoreWithGlobalID(GLOBAL_LEADERBOARD_ID, _scoreButton.Score, (bool _success, string _error) => {

            if (_success)
            {
                Debug.Log(string.Format("Request to report score to leaderboard with GID= {0} finished successfully.", GLOBAL_LEADERBOARD_ID));
                Debug.Log(string.Format("New score= {0}.", _scoreButton.Score));
            }
            else
            {
                Debug.Log(string.Format("Request to report score to leaderboard with GID= {0} failed.", GLOBAL_LEADERBOARD_ID));
                Debug.Log(string.Format("Error= {0}.", _error.GetPrintableString()));
            }
        });
    }
}
