﻿using System;
using UnityEngine;

public class Main : MonoBehaviour
{
    public bool AreServicesAvailable;
    public bool IsUserAuthenticated;

    void Awake ()
    {
        CheckIfServicesAvailable();
        CheckIfUserAuthenticated();
    }

    private void CheckIfUserAuthenticated()
    {
        IsUserAuthenticated = NPBinding.GameServices.LocalUser.IsAuthenticated;
    }

    private void CheckIfServicesAvailable()
    {
        AreServicesAvailable = NPBinding.GameServices.IsAvailable();
        Debug.Log("Google play services available: " + AreServicesAvailable);
    }
}
