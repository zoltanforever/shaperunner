﻿using UnityEngine;
using UnityEngine.UI;

public enum ShapeType
{
    Triangle,
    Square
}

public class Shape : MonoBehaviour {
    public ShapeType Type;

    [HideInInspector]
    public Image Image;

    public RectTransform RectTransform
    {
        get;
        set;
    }

    private void Awake()
    {
        Image = GetComponent<Image>();
        RectTransform = GetComponent<RectTransform>();
    }

    public void SetPosition(float x, float y)
    {
        RectTransform.localPosition = new Vector2(x, y);
    }

    public void MoveDownBy(float y)
    {
        RectTransform.localPosition = RectTransform.transform.localPosition - new Vector3(0, y);
    }

}
