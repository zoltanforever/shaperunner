﻿

public delegate void ShapeButtonClicked();

public interface IButtonEvents  {
    event ShapeButtonClicked TriangleButtonClicked;
    event ShapeButtonClicked SquareButtonClicked;
}
