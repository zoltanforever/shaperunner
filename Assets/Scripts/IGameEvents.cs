﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void GameEventHandler();

public interface IGameEvents {
    event GameEventHandler Paused;
    event GameEventHandler Unpaused;
    event GameEventHandler GameOver;
}
