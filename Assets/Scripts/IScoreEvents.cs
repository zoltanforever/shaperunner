﻿
public delegate void ScoreEventHandler(int score);
public interface IScoreEvents
{
    event ScoreEventHandler ScoreAtGameEnd;
}