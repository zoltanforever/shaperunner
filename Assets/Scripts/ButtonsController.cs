﻿using UnityEngine;
using UnityEngine.UI;


public class ButtonsController: MonoBehaviour, IButtonEvents
{

    public Transform ButtonsTransform;

    Button _leftButton;
    Button _rightButton;

    public event ShapeButtonClicked TriangleButtonClicked;
    public event ShapeButtonClicked SquareButtonClicked;

    void Awake()
    {
        _leftButton = ButtonsTransform.Find("LeftButton").GetComponent<Button>();
        _rightButton = ButtonsTransform.Find("RightButton").GetComponent<Button>();

        _rightButton.onClick.AddListener(RightClicked);
        _leftButton.onClick.AddListener(LeftClicked);
    }
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
        {
            LeftClicked();
        }
        if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            RightClicked();
        }
    }
#endif

    private void LeftClicked()
    {
        Debug.Log("Left button clicked");
        IssueButtonEvent(SquareButtonClicked);
    }

    private void RightClicked()
    {
        Debug.Log("Right button clicked");
        IssueButtonEvent(TriangleButtonClicked);
    }

    private void IssueButtonEvent(ShapeButtonClicked eventToIssue)
    {
        if (eventToIssue != null)
        {
            eventToIssue();
        }
    }
}
