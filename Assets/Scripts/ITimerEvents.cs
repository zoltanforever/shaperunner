﻿
public delegate void TimerEventsHandler();
internal interface ITimerEvents
{
    event TimerEventsHandler TimeOut;
}