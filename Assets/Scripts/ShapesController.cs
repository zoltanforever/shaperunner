﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShapesController : MonoBehaviour, IShapeEvents {

    public Transform ShapeContainer;
    public Shape Triangle;
    public Shape Square;

    public Transform RightEndPoint;
    public Transform LeftEndPoint;

    public event ShapeSelectedHandler CorrectShapeSelected;
    public event ShapeSelectedHandler IncorrectShapeSelected;
    public event ShapeSelectedHandler FirstShapeSelected;

    private const int NUM_SHAPES = 8;
    private const int PADDING = 20;

    private List<Shape> _shapes;
    private RectTransform _bottomShapePosition;
    private Transform _shapeContainer;
    private IButtonEvents _buttonEvents;

    private float _shapeHeight = 0;
    private bool _isFirstShape = true;


    private void Awake()
    {
        _shapes = new List<Shape>();

        _bottomShapePosition = ShapeContainer.Find("BottomShapePosition").GetComponent<RectTransform>();
        _shapeContainer = ShapeContainer.Find("ShapeContainer");

        _buttonEvents = GameObject.FindObjectOfType<ButtonsController>();
        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();

        AddButtonListeners();
        gameEvents.GameOver += Reset;
        gameEvents.Paused += OnPaused;
        gameEvents.Unpaused += OnUnpaused;
    }

    public ShapeType CurrentShape()
    {
        return _shapes[0].Type;
    }

    private void OnUnpaused()
    {
        AddButtonListeners();
        Reset();
    }

    private void OnPaused()
    {
        RemoveButtonListeners();
    }

    private void Start()
    {
        CreateShapes();
    }

    private void AddButtonListeners()
    {
        _buttonEvents.SquareButtonClicked += SquareSelected;
        _buttonEvents.TriangleButtonClicked += TriangleSelected;
    }

    private void RemoveButtonListeners()
    {
        _buttonEvents.SquareButtonClicked -= SquareSelected;
        _buttonEvents.TriangleButtonClicked -= TriangleSelected;
    }

    private void ShapeSelected(ShapeType type)
    {
        if (CurrentShape() == type)
        {
            if (_isFirstShape)
            {
                IssueShapeEvent(FirstShapeSelected);
                _isFirstShape = false;
            }
            IssueShapeEvent(CorrectShapeSelected);

            NextShape();
        }
        else
        {
            IssueShapeEvent(IncorrectShapeSelected);         
        }
    }

    private void IssueShapeEvent(ShapeSelectedHandler shapeSelected)
    {
        if (shapeSelected != null)
        {
            shapeSelected();
        }
    }

    private void SquareSelected()
    {
        ShapeSelected(ShapeType.Square);
    }

    private void TriangleSelected()
    {
        ShapeSelected(ShapeType.Triangle);
    }

    private void NextShape()
    {
        DestroyBottomShape();
        MoveShapesDown();
        CreateNewShapeAtTheTop();
    }

    private void Reset()
    {
        foreach(Shape s in _shapes)
        {
            GameObject.Destroy(s.gameObject);
        }
        _shapes = new List<Shape>();
        CreateShapes();

        _isFirstShape = true;
    }

    private void CreateShapes()
    {
        for (int i = 0; i < NUM_SHAPES; i++)
        {
            Shape shape = InstantiateRandomShape();

            if (_shapeHeight == 0)
            {
                _shapeHeight = shape.Image.rectTransform.rect.height;
            }

            float y = _bottomShapePosition.localPosition.y + i * (_shapeHeight + PADDING);

            shape.SetPosition(0, y);

            _shapes.Add(shape);
        }
    }

    private Shape InstantiateRandomShape()
    {
        if (UnityEngine.Random.Range(0, 2) == 1)
        {
            return UnityEngine.Object.Instantiate(Triangle, _shapeContainer);
        } 
        else
        {
            return UnityEngine.Object.Instantiate(Square, _shapeContainer);
        }
    }

    private void DestroyBottomShape()
    {
        Shape toRemove = _shapes[0];
        _shapes.RemoveAt(0);
        toRemove.transform.DOMove(GetNewParentForShape(toRemove).position, 0.5f);
        toRemove.transform.DOScale(0.5f, 0.5f).OnComplete(() => DestroyShape(toRemove));
    }

    private void DestroyShape(Shape shape)
    {
        GameObject.Destroy(shape.gameObject);
    }

    private Transform GetNewParentForShape(Shape shape)
    {
        if (shape.Type == ShapeType.Triangle)
        {
            return RightEndPoint;
        }
        else
        {
            return LeftEndPoint;
        }
    }

    private void CreateNewShapeAtTheTop()
    {
        float topShapeY = _shapes[_shapes.Count - 1].transform.localPosition.y + _shapeHeight + PADDING;

        Shape newShape = InstantiateRandomShape();
        newShape.SetPosition(0, topShapeY);

        _shapes.Add(newShape);
    }

    private void MoveShapesDown()
    {
        foreach(Shape s in _shapes)
        {
            s.MoveDownBy(PADDING + _shapeHeight);
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            NextShape();
        }
    }
#endif
}