﻿
public delegate void ShapeSelectedHandler();

interface IShapeEvents
{
    event ShapeSelectedHandler CorrectShapeSelected;
    event ShapeSelectedHandler IncorrectShapeSelected;
    event ShapeSelectedHandler FirstShapeSelected;

    ShapeType CurrentShape();
}

