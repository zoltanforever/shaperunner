﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour, IGameEvents
{
    private const int POSSIBLE_CONTINUES = 3;

    private const string CONTINUES_MESSAGE = "You have {0} continues left";
    public GameObject ContinuePromptTransform;
    public Button ExitButton;
    public Button ContinueButton;
    public TMPro.TextMeshProUGUI ContinueText;

    IShapeEvents _shapes;
    IButtonEvents _buttons;
    TimerController _timer;

    public event GameEventHandler Paused;
    public event GameEventHandler Unpaused;
    public event GameEventHandler GameOver;
    
    private int _continuesLeft = POSSIBLE_CONTINUES;

    private bool _isPaused = false;
    
    void Awake () {
        _shapes = GameObject.FindObjectOfType<ShapesController>();
        _shapes.IncorrectShapeSelected += Pause;

        IAddPromptEvents addPrompt = GameObject.FindObjectOfType<AdPromptController>();
        addPrompt.AdFinished += ShowContinuePrompt;
        addPrompt.CancelClicked += EndGame;
        
        _timer = GameObject.FindObjectOfType<TimerController>();
        _timer.TimeOut += Pause;

        ExitButton.onClick.AddListener(EndGame);
        ContinueButton.onClick.AddListener(ContinueGame);

        HideContinuePrompt();
    }

    private void EndGame()
    {     
        IssueGameEvent(GameOver);
        SceneService.LoadScene(Scenes.Menu);
    }

    private void ShowContinuePrompt()
    {
        if (_continuesLeft > 0)
        {
            ContinueText.text = string.Format(CONTINUES_MESSAGE, _continuesLeft);
            ContinuePromptTransform.SetActive(true);
        }
        else
        {
            EndGame();
        }
    }

    private void HideContinuePrompt()
    {
        ContinuePromptTransform.SetActive(false);
    }

    private void ContinueGame()
    {
        HideContinuePrompt();
        _continuesLeft--;
        UnpauseGame();
    }

    public void UnpauseGame()
    {
        _isPaused = false;
        IssueGameEvent(Unpaused);
    }

    private void Pause()
    {
        _isPaused = true;
        IssueGameEvent(Paused);
    }

    private void IssueGameEvent(GameEventHandler gameEventHandler)
    {
        if (gameEventHandler != null)
        {
            gameEventHandler();
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.O))
        {
            _timer.TimeOut -= Pause;
        }
    }
#endif
}
