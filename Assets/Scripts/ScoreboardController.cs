﻿using System;
using TMPro;
using UnityEngine;

public class ScoreboardController : MonoBehaviour, IScoreEvents {

    public Transform ScoreBoardTransform;

    TextMeshProUGUI _scoreText;

    int _currentScore = 0;

    public event ScoreEventHandler ScoreChanged;
    public event ScoreEventHandler ScoreAtGameEnd;

    private void Awake()
    {
        _scoreText = ScoreBoardTransform.Find("ScoreText").GetComponent<TextMeshProUGUI>();

        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();

        IShapeEvents shapeEvents = GameObject.FindObjectOfType<ShapesController>();
        shapeEvents.CorrectShapeSelected += IncrementScore;

        gameEvents.GameOver += OnRoundOver;
    }

    private void IncrementScore()
    {
        _currentScore++;
        UpdateScoreText();
    }

    private void OnRoundOver()
    {
        if (ScoreAtGameEnd != null)
        {
            ScoreAtGameEnd(_currentScore);
        }

        CheckIfHighscore();

        _currentScore = 0;
        UpdateScoreText();
    }

    private void CheckIfHighscore()
    {
        if(_currentScore > ScoreService.GetHighscore())
        {
            ScoreService.SetHighscore(_currentScore);
        }
    }

    private void UpdateScoreText()
    {
        _scoreText.SetText("Score: " + _currentScore);
    }
}
