﻿using UnityEngine;

public class AHundredShapes : AchievementTracker
{

    IShapeEvents _shapeEvents;

    const int SHAPES_REQUIRED = 100;
    int _shapesMatched = 0;

    protected override string GetGlobalAchievementID()
    {
        return "MATCH_100";
    }

    private void Awake()
    {
        _shapeEvents = GameObject.FindObjectOfType<ShapesController>();
        _shapeEvents.CorrectShapeSelected += OnCorrectShapeSelected;

        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();
        gameEvents.GameOver += OnGameOver;
        _shapesMatched = 0;
    }

    private void OnGameOver()
    {
        _shapesMatched = 0;
    }

    private void OnCorrectShapeSelected()
    {
        _shapesMatched += 1;
        if (_shapesMatched == SHAPES_REQUIRED)
        {
            AchieveFull();
        }
    }
}
