﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementTracker : MonoBehaviour {

    virtual protected string GetGlobalAchievementID()
    {
        throw new NotImplementedException();
    }

    protected void AchieveFull()
    {
        AchievePercentage(100f);
    }

    protected void AchievePercentage(double percentage)
    {
        Debug.Log("Achieving percengtage " + percentage);
        NPBinding.GameServices.ReportProgressWithGlobalID(GetGlobalAchievementID(), percentage, OnComplete);
        if (percentage >= 100)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnComplete(bool _success, string _error)
    {
        if (_success)
        {
            Debug.Log("Achievement report successful.");
        }
        else
        {
            Debug.Log("Achievement report failed beacuse " + _error);
        }
    }
}
