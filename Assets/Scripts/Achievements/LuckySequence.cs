﻿using UnityEngine;

public class LuckySequence: AchievementTracker {

    protected override string GetGlobalAchievementID()
    {
        return "LUCKY_SEQUENCE";
    }

    IShapeEvents _shapeEvents;

    private int _counter = 0;
    private ShapeType _previousShape;
    private int COUNTER_ACHIEVEMENT_TARGET = 10;

    // Use this for initialization
    private void Awake() {
        _shapeEvents = GameObject.FindObjectOfType<ShapesController>();
        _shapeEvents.CorrectShapeSelected += OnCorrectShapeSelected;
        _shapeEvents.IncorrectShapeSelected += OnIncorrectShapeSelected;
	}

    private void OnDisable()
    {
        _shapeEvents.CorrectShapeSelected -= OnCorrectShapeSelected;
        _shapeEvents.IncorrectShapeSelected -= OnIncorrectShapeSelected;
    }

    private void OnIncorrectShapeSelected()
    {
        _counter = 0;
    }

    private void OnCorrectShapeSelected()
    {
        if (_previousShape == _shapeEvents.CurrentShape())
        {
            _counter++;
            _previousShape = _shapeEvents.CurrentShape();
        }
        else
        {
            _counter = 0;
        }
        
        if (_counter == COUNTER_ACHIEVEMENT_TARGET)
        {
            AchieveFull();
        }
    }
}
