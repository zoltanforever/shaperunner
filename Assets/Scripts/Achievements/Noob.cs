﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noob : AchievementTracker {

    protected override string GetGlobalAchievementID()
    {
        return "FIRST_ACHIEVEMENT";
    }

    private void Awake()
    {
        AchieveFull();
    }
}
