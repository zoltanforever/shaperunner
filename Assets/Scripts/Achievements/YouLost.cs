﻿using UnityEngine;

public class YouLost : AchievementTracker {
    IGameEvents _gameEvents;

    protected override string GetGlobalAchievementID()
    {
        return "YOU_LOST";
    }

     private void Awake () {
        _gameEvents = GameObject.FindObjectOfType<GameController>();
        _gameEvents.GameOver += AchieveFull;
     }

     private void OnDisable()
     {
        _gameEvents.GameOver -= AchieveFull;
     }
}
