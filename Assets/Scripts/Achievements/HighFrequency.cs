﻿using System.Collections.Generic;
using UnityEngine;

public class HighFrequency : AchievementTracker {

    private const int CORRECT_REQUIRED = 20;
    private const float TIME_PERIOD = 7;

    IShapeEvents _shapeEvents;

    protected override string GetGlobalAchievementID()
    {
        return "HIGH_FREQUENCY";
    }

    private List<float> times;

    private void Awake()
    {
        _shapeEvents = GameObject.FindObjectOfType<ShapesController>();
        _shapeEvents.CorrectShapeSelected += OnCorrectShapeSelected;
        _shapeEvents.IncorrectShapeSelected += OnIncorrectShapeSelected;

        times = new List<float>();
    }

    private void OnDisable()
    {
        _shapeEvents.CorrectShapeSelected -= OnCorrectShapeSelected;
        _shapeEvents.IncorrectShapeSelected -= OnIncorrectShapeSelected;
    }

    private void OnIncorrectShapeSelected()
    {
        times.Clear();
    }

    private void OnCorrectShapeSelected()
    {
        times.Add(Time.time);
        if (IsHighFrequency())
        {
            AchieveFull();
        }
    }

    private bool IsHighFrequency()
    {
        if (times.Count < CORRECT_REQUIRED)
        {
            return false;
        }

        if (times[0] - times[times.Count - 1] < TIME_PERIOD)
        {
            return true;
        }

        return false;
    }
}
