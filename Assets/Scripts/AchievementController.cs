﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.NativePlugins;
using System.Linq;

public class AchievementController : MonoBehaviour {

    private const bool SHOULD_RESET_ACHIEVEMENTS_IN_EDITOR = true;

    private static Achievement[] _achievements;

    private Dictionary<string, Action> achievementDictionary = new Dictionary<string, Action>();

    bool _createdAchievementTrackers = false;


    // Use this for initialization
    void Awake ()
    {
        CreateAchievementDictionary();
        LoadAchievements();
    }

    private void CreateAchievementDictionary()
    {
        achievementDictionary.Add("YOU_LOST", () => CreateAchievementTracker(typeof(YouLost)));
        achievementDictionary.Add("LUCKY_SEQUENCE", () => CreateAchievementTracker(typeof(LuckySequence)));
        achievementDictionary.Add("HIGH_FREQUENCY", () => CreateAchievementTracker(typeof(HighFrequency)));
        achievementDictionary.Add("MATCH_1000", () => CreateAchievementTracker(typeof(AHundredShapes)));
        achievementDictionary.Add("FIRST_ACHIEVEMENT", () => CreateAchievementTracker(typeof(Noob)));
    }

    private void CreateAchievementTracker(Type type)
    {
        GameObject achievementTracker = new GameObject(type + "Achievement tracker");
        achievementTracker.AddComponent(type);
        achievementTracker.transform.SetParent(transform);
    }


    private void LoadAchievements()
    {
        NPBinding.GameServices.LoadAchievements(OnAchievementsLoaded);
    }

    private void OnAchievementsLoaded(Achievement[] achievements, string error)
    {
        if (achievements == null)
        {
            Debug.Log("Couldn't load achievement list with error = " + error);
            return;
        }
        _achievements = achievements;
        CreateAchievementTrackers();
    }

    private void CreateAchievementTrackers()
    {
        foreach ( KeyValuePair<string, Action> kv in achievementDictionary ) {
            if (!IsAchievementComplete(kv.Key))
            {
                achievementDictionary[kv.Key].Invoke();
            }
        }    
    }

    private bool IsAchievementComplete(string globalIdentifier)
    {
#if UNITY_EDITOR
        if (SHOULD_RESET_ACHIEVEMENTS_IN_EDITOR)
        {
            return false;
        }
#endif
        foreach ( Achievement a in _achievements)
        {
            if (a.GlobalIdentifier == globalIdentifier && a.Completed)
            {
                return true;
            }
        }
        return false;
    }

    public static Achievement GetAchievement(string globalIdentifier)
    {
        if (_achievements == null || _achievements.Length == 0)
        {
            return null;
        }
        return _achievements.First<Achievement>(a => a.Completed); 
    }

}
