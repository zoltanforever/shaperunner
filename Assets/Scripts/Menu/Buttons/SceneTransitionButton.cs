﻿using UnityEngine;
using UnityEngine.UI;

public class SceneTransitionButton : MonoBehaviour
{
    public Scenes Scene;
    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(ChangeScene);
    }

    private void ChangeScene()
    {
        SceneService.LoadScene(Scene);
    }
}
