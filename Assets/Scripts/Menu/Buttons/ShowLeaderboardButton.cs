﻿using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.NativePlugins;

public class ShowLeaderboardButton : MonoBehaviour {

    public string GlobalLeaderboardId = "FIRST_LEADERBOARD";
    private Button _button;

    private void Awake () {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(ShowLeaderboard);
	}
	
	private void ShowLeaderboard()
    {
        NPBinding.GameServices.ShowLeaderboardUIWithGlobalID(GlobalLeaderboardId, eLeaderboardTimeScope.TODAY, OnComplete);
    }

    private void OnComplete(string _error)
    {
        Debug.Log("Closed leaderboard UI with error " + _error);
    }
}
