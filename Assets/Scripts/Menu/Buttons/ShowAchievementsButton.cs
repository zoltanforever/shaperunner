﻿using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.Utility;

public class ShowAchievementsButton : MonoBehaviour
{
    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(ShowAchievements);
    }

    private void ShowAchievements()
    {
        NPBinding.GameServices.ShowAchievementsUI(OnComplete);
    }

    private void OnComplete(string error)
    {
        Debug.Log("Achievements view dismissed.");
        if (!string.IsNullOrEmpty(error))
        {
            Debug.Log(string.Format("Error= {0}.", error.GetPrintableString()));
        }
    }
}
