﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LogInButton : MonoBehaviour
{
    private const string LOG_IN = "LogIn";
    private const string LOG_OUT = "LogOut";

    private Button _button;
    private TextMeshProUGUI _textComponent;

    private void Awake()
    {
        _button = GetComponent<Button>();
        _textComponent = GetComponentInChildren<TextMeshProUGUI>();
    }

	void Start ()
    {
        SetAppropriateText();
        _button.onClick.AddListener(LogInOrOut);
	}

    private void SetAppropriateText()
    {
        if (NPBinding.GameServices.LocalUser.IsAuthenticated)
        {
            _textComponent.text = LOG_OUT;
        }
        else
        {
            _textComponent.text = LOG_IN;
        }
    }

    private void LogInOrOut()
    {
        if (NPBinding.GameServices.LocalUser.IsAuthenticated)
        {
            NPBinding.GameServices.LocalUser.SignOut(OnLogOutComplete);
        }
        else
        {
            NPBinding.GameServices.LocalUser.Authenticate(OnLogInComplete);
        }
    }

    private void OnLogOutComplete(bool success, string error)
    {
        if (success)
        {
            Debug.Log("Sign-Out Successfully");
            Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());
            SetAppropriateText();
        }
        else
        {
            Debug.Log("Sign-Out Failed with error " + error);
        }
    }

    private void OnLogInComplete(bool success, string error)
    {
        if (success)
        {
            Debug.Log("Sign-In Successfully");
            Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());
            SetAppropriateText();
        }
        else
        {
            Debug.Log("Sign-In Failed with error " + error);
        }
    }
}
