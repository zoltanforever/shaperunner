﻿using UnityEngine.SceneManagement;

public class SceneService
{
    public static void LoadScene(Scenes scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }
}

public enum Scenes
{
    Main,
    Menu,
    Settings
}