﻿using UnityEngine;

public class GlobalSettings
{
    private const string VOLUME = "Volume";
    private const float DEFAULT_VOLUME = 1;

    public static float Volume
    {
        get
        {
            return PlayerPrefs.GetFloat(VOLUME, DEFAULT_VOLUME);
        }

        set
        {
            PlayerPrefs.SetFloat(VOLUME, value);
        }
    }
}
