﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public AudioSource AudioSource;
    public Slider VolumeSlider;

    private void Awake()
    {
        VolumeSlider.value = GlobalSettings.Volume;
        VolumeSlider.onValueChanged.AddListener(ChangeVolumeSetting);
    }

    private void ChangeVolumeSetting(float newVolume)
    {
        GlobalSettings.Volume = newVolume;
        AudioSource.PlayOneShot(AudioSource.clip, GlobalSettings.Volume);
    }
}
