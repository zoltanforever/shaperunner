﻿using System;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.NativePlugins;

public class MenuController : MonoBehaviour
{
    private const string FIRST_TIME_PLAYING = "FirstTime";

    public TMPro.TextMeshProUGUI ScoreTextComponent;
    public Button AchievementsButton;
    public Button LeaderboardButton;

    private LocalUser _localUser;

    private void Awake()
    {
        _localUser = NPBinding.GameServices.LocalUser;
    }

	private void Start ()
    {
        CheckIfNeedsToLogin();
        SetHighscore();
	}

    private void SetHighscore()
    {
        ScoreTextComponent.text = ScoreService.GetHighscore().ToString();
    }

    private void Update()
    {
        AchievementsButton.interactable = IsUserAuthenticated();
        LeaderboardButton.interactable = IsUserAuthenticated();
    }

    private void CheckIfNeedsToLogin()
    {
        if (!IsUserAuthenticated() && IsFirstTimePlay())
        {
            LogIn();
        }
    }

    private static bool IsFirstTimePlay()
    {
        return PlayerPrefs.GetInt(FIRST_TIME_PLAYING, 0) > 0;
    }

    private void LogIn()
    {
        NPBinding.GameServices.LocalUser.Authenticate(OnLoginComplete);
    }

    private void OnLoginComplete(bool success, string error)
    {
        if (success)
        {
            Debug.Log("Sign-In Successfully");
            Debug.Log("Local User Details : " + _localUser.ToString());
        }
        else
        {
            Debug.Log("Sign-In Failed with error " + error);
        }
    }

    private bool IsUserAuthenticated()
    {
        return _localUser.IsAuthenticated;
    }
}
