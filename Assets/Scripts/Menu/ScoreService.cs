﻿using UnityEngine;
using VoxelBusters.Utility;

public class ScoreService
{
    private const string HIGHSCORE = "Highscore";

    private const string GLOBAL_LEADERBOARD_ID = "FIRST_LEADERBOARD";

    public static int GetHighscore()
    {
        return PlayerPrefs.GetInt(HIGHSCORE, 0);
    }

    public static void SetHighscore(int score)
    {
        PlayerPrefs.SetInt(HIGHSCORE, score);

        if (NPBinding.GameServices.LocalUser.IsAuthenticated)
        {
            ReportScoresToLeaderboard(score);
        }
    }

    private static void ReportScoresToLeaderboard(int score)
    {
        NPBinding.GameServices.ReportScoreWithGlobalID(GLOBAL_LEADERBOARD_ID, score, (bool _success, string _error) => {

            if (_success)
            {
                Debug.Log(string.Format("Request to report score to leaderboard with GID= {0} finished successfully.", GLOBAL_LEADERBOARD_ID));
                Debug.Log(string.Format("New score= {0}.", score));
            }
            else
            {
                Debug.Log(string.Format("Request to report score to leaderboard with GID= {0} failed.", GLOBAL_LEADERBOARD_ID));
                Debug.Log(string.Format("Error= {0}.", _error.GetPrintableString()));
            }
        });
    }
}
