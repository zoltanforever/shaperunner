﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class TimerController : MonoBehaviour, ITimerEvents {

    public Transform TimerContainer;
    public event TimerEventsHandler TimeOut;

    public const float TIME_FOR_SHAPE_IN_SECONDS = 3;
    Image _slider;

    Tweener _sliderTweener;


    private void Awake()
    {
        _slider = TimerContainer.Find("Slider").GetComponent<Image>();

        IGameEvents gameEvents = GameObject.FindObjectOfType<GameController>();
        gameEvents.Paused += StopTimer;

        IShapeEvents shapeEvents = GameObject.FindObjectOfType<ShapesController>();
        shapeEvents.CorrectShapeSelected += StartTimer;
    }

    // Use this for initialization
    private void Start () {
    }

    private void StartTimer()
    {
        if (_sliderTweener == null)
        {
            _sliderTweener = _slider.transform
               .DOLocalMoveX(-_slider.rectTransform.rect.width, TIME_FOR_SHAPE_IN_SECONDS)
               .SetLoops(-1)
               .SetEase(DG.Tweening.Ease.Linear)
               .OnStepComplete(DispatchTimeOut);
        }
        else
        {
            _sliderTweener.Restart();
        }
       
    }

    private void DispatchTimeOut()
    {
        if (TimeOut != null)
        {
            TimeOut();
        }
        _sliderTweener.Pause();
    }

    public void StopTimer()
    {
        if (_sliderTweener != null)
        {
            _sliderTweener.Restart();
            _sliderTweener.Pause();
        }
    }
}
